from flask_script import Manager
from spiders_app.app import app
from spiders_app.spiders_crud.app_routes import index, viki


manager = Manager(app)

# routes
app.add_url_rule('/', 'index', index)
app.add_url_rule('/viki', 'viki', viki, methods=['GET', 'POST'])

if __name__ == '__main__':
    manager.run()
