from constants import OPERATORS, LOGIC_OPERATIONS
from spiders_app.app import mongo
from flask import render_template, request
from spiders_app.spiders_crud.helpers import Pagination, make_filters


def index():
    return render_template('index.html')


def viki():
    filters = {}
    if request.method == 'POST':
        data = request.get_data()
        filters = make_filters(data)
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=10)
    pagination = Pagination(page=page,
                            per_page=per_page,
                            collection=mongo.db.viki_collection,
                            link=request.url_rule.rule,
                            filters=filters)
    return render_template('viki.html',
                           pagination=pagination,
                           per_page=per_page,
                           operations=OPERATORS.keys(),
                           logic_operators=LOGIC_OPERATIONS.keys(),
                           banned_fields=["_id", "scripts_as_list"])
